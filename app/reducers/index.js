import { combineReducers } from 'redux'
import authentication from './authentication'
import chatroom from './chatroom'


const rootReducer = combineReducers({
    authentication,
    chatroom
});


export default rootReducer