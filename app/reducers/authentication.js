

const authentication = (state = {}, action) => {
    switch (action.type) {
        case 'LOGIN_SUCCESSFUL':
            return {
                ...state,
                firebaseAuth: action.firebaseAuth
            };
        default:
            return state
    }
    
}

export default authentication