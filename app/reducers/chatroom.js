

const chatroom = (state = {}, action) => {
    console.log('action');
    console.log(action);
    switch (action.type) {
        case 'ENTER_OLD_CHATROOM':
            return {
                chatID: action.chatID,
            };
        case 'ENTER_NEW_CHATROOM':
            return {
                chatID: action.chatID,
                targetUID: action.targetUID
            };
        default:
            return state
    }
    
}

export default chatroom;