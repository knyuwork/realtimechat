import { Navigation } from 'react-native-navigation';

import ChatList from './ChatList';
import ChatItem from './ChatList/ChatItem';
import Chatroom from './Chatroom';
import Profile from './Profile';
import Home from './Home';

//Register the component in the function below
export default function registerScreens(store, Provider) {
    //Chat feature
    Navigation.registerComponent('Chatroom', () => Chatroom, store, Provider);
    Navigation.registerComponent('ChatList', () => ChatList, store, Provider);


    //Profile feature
    Navigation.registerComponent('Profile', () => Profile, store, Provider);

    //Landing Page
    Navigation.registerComponent('Home', () => Home, store, Provider);
    
    
}
