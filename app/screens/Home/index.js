
import React, { Component } from 'react';
import { View, Text, Image, TouchableOpacity, AsyncStorage } from 'react-native';
import { Navigation } from 'react-native-navigation';
import { connect } from 'react-redux';

import auth0 from '../../config/Auth0Client';
import FirebaseClient from '../../config/FirebaseClient';
import { successfulLogin } from '../../actions/authentication';


import styles from './styles';
const homeBG = require('../../img/homeBG.jpg');
const fBButton = require('../../img/facebookButton.png');



class Home extends Component {

    constructor(props) {
        super(props);

        this.props.navigator.toggleNavBar({
            to: 'hidden', 
            animated: false
        });
    }

    enterApp() {
        const tabs = [
        {
            label: 'Chat',
            screen: 'ChatList',
            icon: require('../../img/list.png'),
            title: 'Chat',

        }, 
        {
            label: 'Profile',
            screen: 'Profile',
            icon: require('../../img/list.png'),
            title: 'Profile',

        }];


        Navigation.startTabBasedApp({
            tabs,
            tabsStyle: {
                tabBarBackgroundColor: '#003a66',
                navBarButtonColor: '#ffffff',
                tabBarButtonColor: '#ffffff',
                navBarTextColor: '#ffffff',
                tabBarSelectedButtonColor: '#ff505c',
                navigationBarColor: '#003a66',
                navBarBackgroundColor: '#003a66',
                statusBarColor: '#002b4c',
                tabFontFamily: 'BioRhyme-Bold',
            },
            appStyle: {
                tabBarBackgroundColor: '#003a66',
                navBarButtonColor: '#ffffff',
                tabBarButtonColor: '#ffffff',
                navBarTextColor: '#ffffff',
                tabBarSelectedButtonColor: '#ff505c',
                navigationBarColor: '#003a66',
                navBarBackgroundColor: '#003a66',
                statusBarColor: '#002b4c',
                tabFontFamily: 'BioRhyme-Bold',
            },
            /*
            drawer: {
                left: {
                    screen: 'example.Types.Drawer'
                }
            }
            */
        });
    }


    fbLogin() {
        
        
        auth0
        .webAuth
        .authorize({scope: 'openid email', audience: 'https://yelperzteam.auth0.com/userinfo'})
        .then(authResult => {
            console.log(JSON.stringify(authResult));

            var body = {
                id_token : authResult.idToken,
                api_type : 'firebase',
                scope : 'openid',
                client_id : 'HFZ0zXajAefS21ZXh_vI_mBZs46ttzP4',
                target : 'HFZ0zXajAefS21ZXh_vI_mBZs46ttzP4',
                grant_type : 'urn:ietf:params:oauth:grant-type:jwt-bearer'
            };

            try {
                fetch('https://'+auth0.auth.domain+'/delegation', {
                    method: 'POST',
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json',
                    }, 
                    body: JSON.stringify(body)})
                .then((response) => response.json())
                .then(res => {

                    FirebaseClient.auth().signInWithCustomToken(res.id_token).then((res) => {
                        let firebaseUserInfo = FirebaseClient.auth().currentUser;

                        //console.log(JSON.stringify(firebaseUserInfo));
                        this.props.successfulLogin(firebaseUserInfo);
                        this.enterApp();

                    }).catch(function(error) {
                        console.log('Failed to sign in Firebase: ' + error);
                    });
                    console.log('Success! firebase signInWithCustomToken');

                });
            } catch(error) { console.error(error); }
            
        })
        .catch(error => console.log(error));
        
        /*
        //TODO
        let userUID = "iv2KeEjgkhb5NKNzdvxOhaU1fF53";
        this.props.successfulLogin(userUID);

        this.enterApp();
        */
        
    }

    render() {
        return (
            <Image
                source={homeBG}
                style={styles.container}>

                <TouchableOpacity style={{alignItems: 'center'}} onPress={() => this.fbLogin()} >
                    <Image source={fBButton}/>
                </TouchableOpacity> 

            </Image>
        );
    }
}


const mapDispatchToProps = (dispatch) => {
	return {
		successfulLogin: (userUID) => {
			dispatch(successfulLogin(userUID))
		}
	}
}

export default connect(null, mapDispatchToProps)(Home)