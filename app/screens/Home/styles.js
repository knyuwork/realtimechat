
const React = require('react-native');

const { StyleSheet, Image, Dimensions } = React;
const { width, height } = Dimensions.get('window');


module.exports = StyleSheet.create({
    container: {
        flex: 1,
        width: undefined,
        height: undefined,
        backgroundColor:'transparent',
        justifyContent: 'center',
        alignItems: 'center',
    }
});