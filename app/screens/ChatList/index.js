import React from 'react';
import {
	Platform,
	StyleSheet,
	Text,
	View,
	ScrollView,
	ListView,
	AsyncStorage,
	TouchableOpacity
} from 'react-native';
import { connect } from 'react-redux';

import ChatItem from './ChatItem';
import FirebaseClient from '../../config/FirebaseClient';


class ChatList extends React.Component {


	constructor(props) {
		super(props);

		this.state = {
			list: [],
		};

		this.chatListListener = this.chatListListener.bind(this);
		
		let userUID = this.props.userUID;
		this.chatListListener(userUID);


	}


	chatListListener(uid) {
		var items = [];

		chatListRef = FirebaseClient.database().ref('/Users/'+uid+'/chatList')
		chatListRef.on('value', snapshot => {
			// get children as an array
			snapshot.forEach((child) => {
				items.push(child.val());
			});

			this.setState({list: items});

		});

	}

	render() {
		return (
			<ScrollView style={{backgroundColor: 'white'}}>
				{
					this.state.list.map(function(chatID, index){
						return (
							<View key={index} style={{borderBottomWidth: 1, borderBottomColor: 'rgba(225, 225, 225, 1)', paddingVertical: 10, paddingLeft: 15}}>
								<ChatItem {...this.props} chatID={chatID}/>
							</View>
						)
					}.bind(this))
				}
			</ScrollView>
		);
	}
}


const styles = StyleSheet.create({
	footerContainer: {
		marginTop: 5,
		marginLeft: 10,
		marginRight: 10,
		marginBottom: 10,
	},
	footerText: {
		fontSize: 14,
		color: '#aaa',
	},
});

function mapStateToProps(state) {
	return {
		userUID: state.authentication.firebaseAuth.uid
	};
}

export default connect(mapStateToProps)(ChatList)
