
const React = require('react-native');

const { StyleSheet, Image, Dimensions } = React;
const { width, height } = Dimensions.get('window');


module.exports = StyleSheet.create({
    /* Font */
    titleFont: {
        fontSize: 17
    },
    chatFont: {
        fontSize: 13,
        color: 'grey'
    },
    lastMsgTimeFont: {

    },
    userIcon: {
        width: width/6, 
        height: width/6, 
        borderRadius: width/12, 
        shadowOpacity: 0.8,
        shadowRadius: 2,
    },
    container: {
        flex:1, 
        flexDirection: 'row'
    },
    contentContainer: {
        marginLeft: 15,
        marginRight: 15,
        flex: 3, 
        justifyContent: 'space-between',
        alignItems: 'flex-start',
    }
});