
import React, { Component } from 'react';
import { Image, View, TouchableOpacity, Text, StyleSheet, Dimensions, AsyncStorage } from 'react-native';
import { connect } from 'react-redux';

import { enterChatroom } from '../../../actions/chatroom';
import FirebaseClient from '../../../config/FirebaseClient';
import styles from './styles';

const { width, height } = Dimensions.get('window');
const testImage = require('./img/testImage.png');

class ChatItem extends Component {

    state = {
        chatTitle: "",
        lastMessage: ""
	}

    constructor(props) {
        super(props);

        let { chatID, userUID } = this.props;

        //Get the metadata of the chatroom
		chatroomRef = FirebaseClient.database().ref('/Chatrooms/'+chatID+'/metadata');
        chatroomRef.on('value', (snap) => {
            let userList = snap.child('users').val();

            //Set the chatroom title with the target user name
            Object.keys(userList).forEach((uID) => {
                if (uID != userUID) {
                    const userNameRef = FirebaseClient.database().ref('/Users/'+uID+'/name');
                    userNameRef.on('value', (name) => {
                        this.setState({chatTitle: name.val()});

                    });
                }
                
            });

            let lastMessage = snap.child('lastMessage').val().text;
            this.setState({lastMessage});
            
            

		});

    }


    onPress = () => {
        let { chatID } = this.props;
        this.props.enterChatroom(this.props.navigator, chatID);
    }

    render() {
        return (

            <TouchableOpacity onPress={() => this.onPress()}>  
                <View style={styles.container}>
                    <Image style={styles.userIcon} source={testImage} />
                    <View style={styles.contentContainer}>
                        <Text style={styles.titleFont}>{this.state.chatTitle}</Text>
                        <Text style={styles.chatFont}>{this.state.lastMessage}</Text>
                    </View>
                </View>
            </TouchableOpacity>  
        );
    }
}

function mapStateToProps(state) {
	return {
		userUID: state.authentication.firebaseAuth.uid
	};
}

const mapDispatchToProps = (dispatch) => {
	return {
		enterChatroom: (navigator, chatID, targetUID) => {
			dispatch(enterChatroom(navigator, chatID, targetUID))
		}
	}
}


export default connect(mapStateToProps, mapDispatchToProps)(ChatItem)