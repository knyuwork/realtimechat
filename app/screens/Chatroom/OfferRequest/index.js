import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Image,
  Dimensions,
  AsyncStorage
} from 'react-native';
import styles from './styles';
import FirebaseClient from '../../../config/FirebaseClient';
//import { acceptTask } from '../../function';
const { width, height } = Dimensions.get('window');


export default class OfferRequest extends Component {


    constructor(props) {
        super(props);
        this.state = {
            name: "Peter",
            chatId: "tasks-1493273824404_Matthew_Petter",
            taskTitle: "TaskTitle"
        }

        this.sendRequestResponse = this.sendRequestResponse.bind(this);
        /*
        AsyncStorage.getItem('CurrentChat').then((currentChat) => {   
            let currentChatObj = JSON.parse(currentChat);
            this.setState({
                chatId: currentChatObj.chatId,
                taskTitle: currentChatObj.taskTitle
            });
        }).done();
        AsyncStorage.getItem('Profile').then((value) => {   
            console.log("TEST: "+ value);
            let name = JSON.parse(value).name;
            this.setState({name});
        }).done();
        console.log("OfferRequest: "+ JSON.stringify(props.currentMessage));
        */
    }

    sendRequestResponse(response) {
        
        var chatId = this.state.chatId;
        var name = this.state.name;
        var date = (new Date()).toString();
        chatRef = FirebaseClient.database().ref('/Chat/'+chatId+'/chatContent');
        chatRef.orderByChild("_id").equalTo("RECEIVE_NEW_OFFER").once("value", function(snapshot) {
            console.log(JSON.stringify(Object.keys(snapshot.val())[0]));
            let key = JSON.stringify(Object.keys(snapshot.val())[0]);
            FirebaseClient.database().ref('/Chat/'+chatId+'/chatContent/'+JSON.parse(key)).set({
                _id: "RECEIVE_NEW_OFFER",
                text: response,
                createdAt: date,
                user: {
                    _id: name,
                    name: 'React Native',
                    avatar: 'https://facebook.github.io/react/img/logo_og.png',
                }
            });
            
        });
    }

    onAccept() {
        var chatId = this.state.chatId;
        var name = this.state.name;
        var date = (new Date()).toString();
        chatRef = FirebaseClient.database().ref('/Chat/'+chatId);
        chatRef.once('value', (res) => {
            //TODO
            //console.log(JSON.stringify(res.val().taskId));
            //acceptTask(name, res.val().taskId);
        });
        this.sendRequestResponse("Accept");
    }

    onDecline() {
        this.sendRequestResponse("Decline");
    }

    renderAcceptance() {
        let text = this.props.currentMessage.text;
        if ( text == "Offer") {
            if (this.state.name == this.props.currentMessage.user._id) {
                return (
                    <View style={[styles.flex_1, {flexDirection: 'row'}]}>
                        <Text style={{color: 'white'}}>Waiting for helper to respond...</Text>
                    </View>
                );
            } else {
                return (

                    <View style={[styles.flex_1, {flexDirection: 'row'}]}>
                        <View style={[styles.flex_1]}>
                            <TouchableOpacity style={[styles.button]} onPress={() => this.onAccept()}>
                                <Text style={{color: 'white'}}>Accept</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={[styles.flex_1]}>
                            <TouchableOpacity style={[styles.button]} onPress={() => this.onDecline()}>
                                <Text style={{color: 'white'}}>Decline</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                );

            }

        } else if (text == "Accept") {
            return (
                <View style={[styles.flex_1, {flexDirection: 'row'}]}>
                    <Text style={{color: 'white'}}>Offer is </Text><Text style={{color: 'green'}}>Accepted</Text>
                </View>
            );
        } else if (text == "Decline") {
            return (
                <View style={[styles.flex_1, {flexDirection: 'row'}]}>
                    <Text style={{color: 'white'}}>Offer is </Text><Text style={{color: 'red'}}>Declined</Text>
                </View>
            );
        }
    }

    render() {
        return (

            <View style={{justifyContent: 'center', alignItems: 'center', marginVertical: 15}}>
                <View style={{backgroundColor: 'rgba(137,138,139,1)', borderRadius: 10, height: 140, width: width*9/10}}>
                    <View style={[styles.flex_1, {borderBottomWidth: 1, borderBottomColor: 'white'}]}>
                        <Text style={{fontSize: 16, color: 'white'}}>Receive New Offer</Text>
                    </View>
                    <View style={styles.flex_1}>
                        <Text style={{fontSize: 16, color: 'white'}}>Yeller offers you a request</Text>
                    </View>
                    {this.renderAcceptance()}
                </View>
            </View>
        );
    }

}
