
const React = require('react-native');

const { StyleSheet, Image, Dimensions } = React;
const { width, height } = Dimensions.get('window');

module.exports = StyleSheet.create({
    flex_1: {
        flex: 1, 
        alignItems: 'center', 
        justifyContent: 'center'
    },
    button: {
        backgroundColor: 'rgba(87,148,165,1)',
        borderRadius: 10,
        paddingVertical: 5,
        paddingHorizontal: 10
    }
});
