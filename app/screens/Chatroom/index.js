
import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Image,
  Dimensions,
  AsyncStorage
} from 'react-native';
import {GiftedChat, Actions, Bubble, Message} from 'react-native-gifted-chat';
import { connect } from 'react-redux';


import FirebaseClient from '../../config/FirebaseClient';
import { sendMessage } from '../../actions/chatroom';
import CustomActions from './CustomActions';
import CustomView from './CustomView';
import OfferRequest from './OfferRequest';
import styles from './styles';

const testImage = require('./img/testImage.png');
const returnIcon = require('./img/return.png');
const sendRequestIcon = require('./img/sendRequestIcon.png');
const { width, height } = Dimensions.get('window');



class Chatroom extends Component {

	//Component Function
    constructor(props) {
		super(props);
		this.state = {
			messages: [],
			loadEarlier: true,
			typingText: null,
			isLoadingEarlier: false,
			chatId: "tasks-1493273824404_Matthew_Petter",
			name: "Matthew",
			yellerId: "Matthew"

		};

		this._isMounted = false;
		this.renderCustomActions = this.renderCustomActions.bind(this);
		this.renderBubble = this.renderBubble.bind(this);
		this.renderFooter = this.renderFooter.bind(this);
		this.onLoadEarlier = this.onLoadEarlier.bind(this);

		//for firebase
		this.listenForNewMessages = this.listenForNewMessages.bind(this);
		this.sendMessage = this.sendMessage.bind(this);

	}

	componentDidMount() {
		this.listenForNewMessages();
	}

	componentWillMount() {
		this._isMounted = true;
	}

	componentWillUnmount() {
		this._isMounted = false;
	}


	//Listener
	listenForNewMessages() {
		this.props.chatroomFirebaseRef.on('value', (snap) => {
			// get children as an array


			var items = [];
			snap.forEach((child) => {
				let chatObject = child.val();
				chatObject.createdAt = new Date(chatObject.createdAt);
				items.push(chatObject);
			});

			items.reverse();

			this.setState({
				messages: items
			});


		});
	}

	

	sendMessage = (text) => {
		let chatID = this.props.chatID;
		let senderUID = this.props.userUID;
		let targetUID = this.props.targetUID;
		//this.props.sendMessage(text[0].text, this.state.chatID);


        let message = {
            text: text[0].text,
            createdAt: Date.now(),
            user: {        
                _id: senderUID
            },
        };
		
		this.props.sendMessage(message, chatID);

		//If targetUID is not in Redux Store, this chatroom is an old chatroom
		if ( targetUID != null) {
			let targetChatListRef = FirebaseClient.database().ref('/Users/'+targetUID+'/chatList/'+senderUID);
			targetChatListRef.set(chatID);

			let senderChatListRef = FirebaseClient.database().ref('/Users/'+senderUID+'/chatList/'+targetUID);
			senderChatListRef.set(chatID);

			let chatroomUsersRef = FirebaseClient.database().ref('/Chatrooms/'+chatID+'/metadata/users');
			chatroomUsersRef.child(senderUID).set('user');
			chatroomUsersRef.child(targetUID).set('user');
		}

		let chatroomLastMessageRef = FirebaseClient.database().ref('/Chatrooms/'+chatID+'/metadata/lastMessage');
		chatroomLastMessageRef.set(message);
		
	}


	//TODO
	sendRequest() {
		var chatID = this.props.chatID;
		var date = (new Date()).toString();
		var uid = this.uid;
		
		//firebaseRef = FirebaseClient.database().ref('/Chat/'+chatID+'/chatContent');

		this.firebaseRef.orderByChild("_id").equalTo("RECEIVE_NEW_OFFER").once("value", function(snapshot) {
			
			if (snapshot.val()) {
				let key = JSON.stringify(Object.keys(snapshot.val())[0]);
				//FirebaseClient.database().ref('/Chat/'+chatID+'/chatContent/'+JSON.parse(key)).set({
				this.irebaseRef.ref(JSON.parse(key)).set({
					_id: "RECEIVE_NEW_OFFER",
					text: response,
					createdAt: date,
					user: {
						_id: uid,
						name: 'React Native',
						avatar: 'https://facebook.github.io/react/img/logo_og.png',
					}
				});
			} else {
				this.firebaseRef.push({
					_id: "RECEIVE_NEW_OFFER",
					text: "Offer",
					createdAt: date,
					user: {
						_id: uid,
						name: 'React Native',
						avatar: 'https://facebook.github.io/react/img/logo_og.png',
					}
				});

			}
			
		});
	}


  
  	onLoadEarlier() {
		this.setState((previousState) => {
			return {
				isLoadingEarlier: true,
			};
		});

		setTimeout(() => {
			if (this._isMounted === true) {
				this.setState((previousState) => {
					return {
						messages: GiftedChat.prepend(previousState.messages, require('./data/old_messages.js')),
						loadEarlier: false,
						isLoadingEarlier: false,
					};
				});
			}
		}, 1000); // simulating network
	}


	//UI Function
	renderCustomActions(props) {
		if (Platform.OS === 'ios') {
			return (
				<CustomActions
				{...props}
				/>
			);
		}
		const options = {
			'Action 1': (props) => {
				alert('option 1');
			},
			'Action 2': (props) => {
				alert('option 2');
			},
			'Cancel': () => {},
		};
		return (
			<Actions
				{...props}
				options={options}
			/>
		);
	}

	renderBubble(props) {
		return (
			<Bubble
				{...props}
				wrapperStyle={{
					left: {
						backgroundColor: '#f0f0f0',
					}
				}}
			/>
		);
	}

	renderCustomView(props) {
		return (
			<CustomView
				{...props}
			/>
		);
	}

	renderFooter(props) {
		if (this.state.typingText) {
			return (
				<View style={styles.footerContainer}>
				<Text style={styles.footerText}>
					{this.state.typingText}
				</Text>
				</View>
			);
		}
		return null;
	}

	renderMessage(props) {
		if (props.key == "RECEIVE_NEW_OFFER") {
			return (
				<OfferRequest {...props} />
			);
		} else {
			return (
				<Message {...props}/>
			);
		}
	}

	renderSendRequestButton() {
		if (this.state.yellerId == this.state.name) {
			return (
				<TouchableOpacity style={styles.sendRequestButton} onPress={() => this.sendRequest()}>
					<Image style={{width: 30, height: 30,resizeMode: Image.resizeMode.contain}} source={sendRequestIcon}/>
				</TouchableOpacity>
			);
		
		} else {
			return (
				<View style={styles.sendRequestButton}>
				</View>
			);
		}
		
	}

	render() {
		return (
			<GiftedChat
				messages={this.state.messages}
				onSend={this.sendMessage}
				//loadEarlier={this.state.loadEarlier}
				//onLoadEarlier={this.onLoadEarlier}
				//isLoadingEarlier={this.state.isLoadingEarlier}

				user={{
					_id: this.props.userUID, // sent messages should have same user._id
				}}
				disableAvatar={true}
				renderActions={this.renderCustomActions}
				renderBubble={this.renderBubble}
				renderCustomView={this.renderCustomView}
				renderFooter={this.renderFooter}
				renderMessage={this.renderMessage}
			/>
		);
	}
}


function mapStateToProps(state) {
	return {
		chatID: state.chatroom.chatID,
		targetUID: state.chatroom.targetUID,
		userUID: state.authentication.firebaseAuth.uid,
		chatroomFirebaseRef: FirebaseClient.database().ref('/Chatrooms/'+state.chatroom.chatID+'/messages')
	};
}

const mapDispatchToProps = (dispatch) => {
	return {
		sendMessage: (message, chatID) => {
			dispatch(sendMessage(message, chatID))
		}
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(Chatroom)
