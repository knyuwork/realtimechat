
const React = require('react-native');

const { StyleSheet, Image, Dimensions } = React;
const { width, height } = Dimensions.get('window');

module.exports = StyleSheet.create({
    /* Font */
  titleFont: {
    fontSize: 17,
    color: 'white'
  },
  onlineStatusFont: {
    fontSize: 13,
    color: 'rgba(200,200,200,1)'
  },
  /* Component */
  userIcon: {
    width: width/6, 
    height: width/6, 
    borderRadius: width/12, 
    shadowOpacity: 0.8,
    shadowRadius: 2,
  },
  header: {
    backgroundColor: 'rgba(70,131,149,1)', 
    height: 90, 
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  button: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  sendRequestButton: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  container: {
    flexDirection: 'row',
    position: 'relative',
    flex:6
  },
  contentContainer: {
    marginLeft: 15,
    marginRight: 15,
    justifyContent: 'center',
    alignItems: 'flex-start',
  }
});
