import FirebaseClient from '../config/FirebaseClient';
//import DeviceInfo from 'react-native-device-info';
//import FCM, { FCMEvent, NotificationType, WillPresentNotificationResult, RemoteNotificationResult } from 'react-native-fcm';
//import { Platform } from 'react-native';

export const addMessage = (message) => ({
    type: 'ADD_MESSAGE',
    ...message
});


//export const sendMessage = (text, senderUid, receiverUid) => {
export const sendMessage = (message, chatID) => {
    return function (dispatch) {
        const newMessageRef = FirebaseClient.database()
                                .ref('Chatrooms/'+chatID+'/messages')
                                .push();
        message._id = newMessageRef.key;
        newMessageRef.set(message);
        //dispatch(addMessage(message));
    };
};



export const enterOldChatroom = (chatID) => ({
    type: 'ENTER_OLD_CHATROOM',
    chatID: chatID,
});


export const enterNewChatroom = (chatID, targetUID) => ({
    type: 'ENTER_NEW_CHATROOM',
    chatID: chatID,
    targetUID: targetUID
});


export const enterChatroom = (navigator, chatID, targetUID) => {
    console.log('targetUID: '+ targetUID);
    console.log('chatID: '+ chatID);
    return function (dispatch) {
        if (targetUID == undefined) {
            dispatch(enterOldChatroom(chatID));
        } else {
            dispatch(enterNewChatroom(chatID, targetUID));

        }

        navigator.push({
            screen: 'Chatroom', // unique ID registered with Navigation.registerScreen
            animated: true, // does the push have transition animation or does it happen immediately (optional)
            backButtonTitle: undefined, // override the back button title (optional)
            backButtonHidden: false, // hide the back button altogether (optional)
        });

    }
}
