


export const successfulLogin = (firebaseAuth) => ({
    type: 'LOGIN_SUCCESSFUL',
    firebaseAuth
});