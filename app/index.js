import { Platform } from 'react-native';
import { Navigation } from 'react-native-navigation';
import { Provider } from 'react-redux'
import { createStore, applyMiddleware } from 'redux'
import thunk from 'redux-thunk';
import { createLogger } from 'redux-logger';

import rootReducer from './reducers'
import registerScreens from './screens';

// screen related book keeping
let store = createStore(rootReducer,
    applyMiddleware(
        thunk,
        //loggerMiddleware
    )
);

registerScreens(store, Provider);

Navigation.startSingleScreenApp({
    screen: {
        screen: 'Home', // unique ID registered with Navigation.registerScreen
        title: 'Welcome', // title of the screen as appears in the nav bar (optional)
        navigatorStyle: {}, // override the navigator style for the screen, see "Styling the navigator" below (optional)
        navigatorButtons: {} // override the nav buttons for the screen, see "Adding buttons to the navigator" below (optional)
    },
});

